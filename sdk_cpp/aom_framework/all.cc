// GRPC reflection relies on static initialization. However, this
// can (and does) break when no other symbols from a translation unit 
// is used. We get around that by having all proto-generated code be part 
// of the same translation unit.
#include "./protocols.pb.cc"
#include "./services.grpc.pb.cc"
#include "./services.pb.cc"