FROM registry.gitlab.com/age-of-minds/aom-framework:builder

# version tags
ARG PROMETHEUS_CPP_RELEASE_TAG=v0.6.0
ARG SPDLOG_RELEASE_TAG=v1.x
ARG GOOGLE_TEST_RELEASE_TAG=release-1.8.1

RUN apk add --no-cache zlib-dev curl-dev util-linux-dev cmake gdb 

# Install prometheus client library
RUN    echo "--- installing prometheus cpp client ---" && \
    git clone -b ${PROMETHEUS_CPP_RELEASE_TAG} --single-branch --depth=1 https://github.com/jupp0r/prometheus-cpp.git /var/local/src/prometheus-cpp && \
    cd /var/local/src/prometheus-cpp && \
    git submodule update --init --recursive && \
    mkdir _bld && cd _bld && \
    cmake .. && make -j$(nproc) && make install

# Install spdlog
RUN    echo "--- installing spdlog ---" && \
    git clone -b ${SPDLOG_RELEASE_TAG} --single-branch  https://github.com/gabime/spdlog.git /var/local/src/spdlog && \
    cd /var/local/src/spdlog && \
    git submodule update --init --recursive && \
    mkdir _bld && cd _bld && \
    cmake .. -DSPDLOG_BUILD_TESTS=OFF && make -j$(nproc) && make install

# Install googletest
RUN    echo "--- installing Google test ---" && \
    git clone -b ${GOOGLE_TEST_RELEASE_TAG} --single-branch  https://github.com/google/googletest.git /var/local/src/gtest && \
    cd /var/local/src/gtest && \
    git submodule update --init --recursive && \
    mkdir _bld && cd _bld && \
    cmake .. && make -j$(nproc) && make install

COPY --from=namely/grpc-cli /usr/local/bin/grpc_cli /usr/local/bin/