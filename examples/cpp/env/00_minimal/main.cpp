#include "aom_framework/all.h"

#include <grpcpp/grpcpp.h>
#include <spdlog/spdlog.h>


class Env_service : public aom_api::Environment::Service {
public:
    // Called when a new trial is created.
    ::grpc::Status Start(::grpc::ServerContext* context, const ::aom_api::EnvStartRequest* request, ::aom_api::EnvStartReply* response) override {
      spdlog::info("env start");
      return grpc::Status::OK;
    }

    // Called when it's time to advance the simulation time.
    ::grpc::Status Update(::grpc::ServerContext* context, const ::aom_api::EnvUpdateRequest* request, ::aom_api::EnvUpdateReply* response) override {
      return grpc::Status::OK;
    }
};

int main(int argc, const char* argv[]) {

  std::string server_address("0.0.0.0:9003");

  Env_service service;

  grpc::ServerBuilder builder;
  builder.AddListeningPort(server_address, grpc::InsecureServerCredentials());
  builder.RegisterService(&service);
  
  auto server = builder.BuildAndStart();
  
  server->Wait();

  return 0;
}