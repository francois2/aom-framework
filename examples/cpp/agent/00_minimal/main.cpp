#include "aom_framework/all.h"

#include <grpcpp/grpcpp.h>

class Agent_service : public aom_api::Agent::Service {
public:
    // Called when a Trial begins.
    ::grpc::Status Start(::grpc::ServerContext* context, const ::aom_api::AgentStartRequest* request, ::aom_api::AgentStartReply* response) {
      return grpc::Status::OK;
    }
    
    // Called when a request to make a decision based on an environment state.
    ::grpc::Status Decide(::grpc::ServerContext* context, const ::aom_api::AgentDecideRequest* request, ::aom_api::AgentDecideReply* response) {
      return grpc::Status::OK;
    }

    // Called when a judgement has been made on a decision
    ::grpc::Status Reward(::grpc::ServerContext* context, const ::aom_api::AgentRewardRequest* request, ::aom_api::AgentRewardReply* response) {
      return grpc::Status::OK;
    }
};

int main(int argc, const char* argv[]) {
  std::string server_address("0.0.0.0:9002");

  Agent_service service;

  grpc::ServerBuilder builder;
  builder.AddListeningPort(server_address, grpc::InsecureServerCredentials());
  builder.RegisterService(&service);

  auto server = builder.BuildAndStart();

  server->Wait();

  return 0;
}