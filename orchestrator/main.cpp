#include "config.h"

#include <prometheus/exposer.h>
#include <prometheus/registry.h>
#include "aom/completion.h"
#include "aom/orchestrator.h"

#include "slt/settings.h"

#include "spdlog/spdlog.h"

namespace settings {
slt::Setting trial_port =
    slt::Setting_builder<std::uint16_t>()
        .with_default(9000)
        .with_description("The port to listen for trials on")
        .with_env_variable("TRIAL_SERVER_PORT")
        .with_arg("port");

slt::Setting prometheus_port =
    slt::Setting_builder<std::uint16_t>()
        .with_default(8080)
        .with_description("The port to broadcast prometheus metrics on")
        .with_env_variable("PROMETHEUS_PORT")
        .with_arg("prometheus_port");

slt::Setting env_service_endpoint =
    slt::Setting_builder<std::string>()
        .with_default("")
        .with_description("Where the use-case's environment service lives at")
        .with_env_variable("ENV_SERVICE_ENDPOINT")
        .with_arg("env")
        .with_validator([](const auto& v) { return v != ""; });

slt::Setting agent_service_endpoint =
    slt::Setting_builder<std::string>()
        .with_default("")
        .with_description("Where the use-case's agent service lives at")
        .with_env_variable("AGENT_SERVICE_ENDPOINT")
        .with_arg("agent")
        .with_validator([](const auto& v) { return v != ""; });

slt::Setting display_version =
    slt::Setting_builder<bool>()
        .with_default(false)
        .with_description("Display the orchestrator's version and quit")
        .with_arg("version");
}  // namespace settings

int main(int argc, const char* argv[]) {
  slt::Settings_context ctx("aom_orchestrator", argc, argv);

  if (ctx.help_requested()) {
    return 0;
  }

  if (settings::display_version.get()) {
    std::cout << AOM_ORCHESTRATOR_VERSION << "\n";
    return 0;
  }

  ctx.validate_all();

  // ******************* Dependant services *******************

  spdlog::trace("creating stubs");
  spdlog::info("Connecting to env service at {}",
               settings::env_service_endpoint.get());
  spdlog::info("Connecting to agent service at {}",
               settings::agent_service_endpoint.get());

  auto env_stub = aom_api::Environment::NewStub(
      grpc::CreateChannel(settings::env_service_endpoint.get(),
                          grpc::InsecureChannelCredentials()));
  auto agent_stub = aom_api::Agent::NewStub(
      grpc::CreateChannel(settings::agent_service_endpoint.get(),
                          grpc::InsecureChannelCredentials()));

  // ******************* Endpoints *******************
  auto trial_endpoint =
      std::string("0.0.0.0:") + std::to_string(settings::trial_port.get());
  auto prometheus_endpoint =
      std::string("0.0.0.0:") + std::to_string(settings::prometheus_port.get());

  // ******************* Monitoring *******************
  spdlog::trace("starting prometheus");
  prometheus::Exposer ExposePublicParser(prometheus_endpoint);

  // ******************* Server *******************
  grpc::ServerBuilder builder;
  auto cq = builder.AddCompletionQueue();
  spdlog::trace("creating orchestrator");
  aom::Orchestrator orchestrator(*env_stub, *agent_stub, *cq);
  aom_api::Trial::AsyncService trial_service;

  builder.AddListeningPort(trial_endpoint, grpc::InsecureServerCredentials());
  builder.RegisterService(&trial_service);

  spdlog::trace("building server");
  auto server = builder.BuildAndStart();

  orchestrator.register_in_async_service(&trial_service, cq.get());

  spdlog::info("Server listening for trials on {}", trial_endpoint);

  // ******************* Main Loop *******************
  while (true) {
    void* tag = nullptr;
    bool ok = false;

    cq->Next(&tag, &ok);

    if (tag != nullptr) {
      auto as_completion = reinterpret_cast<aom::Completion*>(tag);
      as_completion->exec();
    }
  }

  return 0;
}