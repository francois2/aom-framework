include(GoogleTest)
find_package(GTest REQUIRED CONFIG)

add_executable(orchestrator_tests tests.cpp)
target_link_libraries(orchestrator_tests aom_orchestrator_lib GTest::gtest_main GTest::gtest GTest::gmock)
target_include_directories(orchestrator_tests PRIVATE .)

gtest_discover_tests(orchestrator_tests)


configure_file(smoke_test.sh smoke_test.sh COPYONLY)
add_test(NAME smoke_test COMMAND ./smoke_test.sh)