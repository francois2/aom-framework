#!/bin/bash

# Launch the placeholder services
../../examples/cpp/agent/00_minimal/agent_00_minimal &
AGENT_PID=$!

../../examples/cpp/env/00_minimal/env_00_minimal &
ENV_PID=$!

# Launch the orchestrator services
TRIAL_SERVER_PORT=9001 ENV_SERVICE_ENDPOINT=127.0.0.1:9003 AGENT_SERVICE_ENDPOINT=127.0.0.1:9002 ../aom_orchestrator &
ORCH_PID=$!

# Make sure the orchestrator has had time to start
sleep 1s

# Start a session
grpc_cli call 127.0.0.1:9001 aom_api.Trial.Start ""
SUCCESS=$?


# Cleanup after ourselves. This isn't great, but since this will always run in a container, meh.
kill $ORCH_PID
kill $ENV_PID
kill $AGENT_PID

exit $SUCCESS