#include "gmock/gmock.h"
#include "gtest/gtest.h"

#include "aom_framework/protocols.pb.h"
#include "grpc_mock_utils.h"

// This needs to be done BEFORE we include the client stubs.
ALLOW_ASYNC_REPLY_MOCKING(aom_api::EnvStartReply)
ALLOW_ASYNC_REPLY_MOCKING(aom_api::AgentStartReply)

#include "aom/orchestrator.h"
#include "aom_framework/services_mock.grpc.pb.h"

#include <chrono>
#include <thread>

using namespace std::chrono_literals;

using testing::Return;
using testing::_;

// General purpose test fixture for the orchestrator
class Orchestrator_test : public ::testing::Test {
 protected:
  aom_api::MockEnvironmentStub env;
  aom_api::MockAgentStub agent;
  std::unique_ptr<grpc::CompletionQueue> cq;
  std::unique_ptr<aom::Orchestrator> orchestrator;

  // Call this to execute the content of the server's completion queue.
  void flush_queue() {
    void* tag;
    bool ignored_ok;

    while (cq->Next(&tag, &ignored_ok)) {
      auto as_completion = reinterpret_cast<aom::Completion*>(tag);
      as_completion->exec();
    }
  }

  void SetUp() override {
    cq = std::make_unique<grpc::CompletionQueue>();
    orchestrator = std::make_unique<aom::Orchestrator>(env, agent, *cq);
  }

  void TearDown() override {
    cq->Shutdown();

    void* tag;
    bool ignored_ok;

    while (cq->Next(&tag, &ignored_ok)) {
      auto as_completion = reinterpret_cast<aom::Completion*>(tag);
      delete as_completion;
    }
  }
};

TEST_F(Orchestrator_test, create_and_delete) {
  // Just double-check that the test fixture is behaving correctly
}

TEST_F(Orchestrator_test, start_success) {
  grpc::ServerContext ctx;
  aom_api::TrialStartRequest req;
  aom_api::TrialStartReply rep;

  EXPECT_CALL(env, AsyncStartRaw(_, _, _))
      .Times(1)
      .WillOnce(Return(new FakeAsyncGrpcResponse<aom_api::EnvStartReply>(
          grpc::Status::OK, {})));

  EXPECT_CALL(agent, AsyncStartRaw(_, _, _))
      .Times(1)
      .WillOnce(Return(new FakeAsyncGrpcResponse<aom_api::AgentStartReply>(
          grpc::Status::OK, {})));

  bool done = false;
  orchestrator->handle_Start(
      &ctx, &req, [&done](auto s, auto rep) { EXPECT_TRUE(s.ok()); });
}

TEST_F(Orchestrator_test, start_failure_propagation) {
  grpc::ServerContext ctx;
  aom_api::TrialStartRequest req;
  aom_api::TrialStartReply rep;

  // If the event call fails
  {
    EXPECT_CALL(env, AsyncStartRaw(_, _, _))
        .Times(1)
        .WillOnce(Return(new FakeAsyncGrpcResponse<aom_api::EnvStartReply>(
            grpc::Status::CANCELLED, {})));

    EXPECT_CALL(agent, AsyncStartRaw(_, _, _))
        .Times(1)
        .WillOnce(Return(new FakeAsyncGrpcResponse<aom_api::AgentStartReply>(
            grpc::Status::OK, {})));

    bool done = false;
    orchestrator->handle_Start(
        &ctx, &req, [&done](auto s, auto rep) { EXPECT_FALSE(s.ok()); });
  }

  // If the agent call fails
  {
    EXPECT_CALL(env, AsyncStartRaw(_, _, _))
        .Times(1)
        .WillOnce(Return(new FakeAsyncGrpcResponse<aom_api::EnvStartReply>(
            grpc::Status::OK, {})));

    EXPECT_CALL(agent, AsyncStartRaw(_, _, _))
        .Times(1)
        .WillOnce(Return(new FakeAsyncGrpcResponse<aom_api::AgentStartReply>(
            grpc::Status::CANCELLED, {})));

    orchestrator->handle_Start(&ctx, &req,
                               [](auto s, auto rep) { EXPECT_FALSE(s.ok()); });
  }
}