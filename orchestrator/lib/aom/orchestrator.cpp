#include "aom/orchestrator.h"
#include "aom/completion.h"

#include "spdlog/spdlog.h"

#include <memory>

namespace aom {

class Trial_start_handler
    : public std::enable_shared_from_this<Trial_start_handler> {
 public:
  using cb_t = Orchestrator::Start_cb;
  using req_t = ::aom_api::TrialStartRequest;
  using rep_t = ::aom_api::TrialStartReply;

  Trial_start_handler(Orchestrator* owner, cb_t cb) : owner_(owner), cb_(cb) {
    new_trial_ = std::make_unique<Trial>();
  }

  ~Trial_start_handler() { new_trial_.reset(); }

  void recv() {
    --pending_subcalls;

    if (0 == pending_subcalls) {
      ::aom_api::TrialStartReply reply;
      if (!env_start_status.ok()) {
        cb_(env_start_status, reply);
      } else if (!agent_start_status.ok()) {
        cb_(agent_start_status, reply);
      } else {
        reply.set_player_id(1);
        reply.set_session_id(to_string(new_trial_->id()));

        owner_->trials_.emplace(new_trial_->id(), std::move(new_trial_));
        cb_(grpc::Status::OK, reply);
      }
    }
  }

  void begin(const req_t* req) {
    auto self = this->shared_from_this();
    auto session_id_string = to_string(new_trial_->id());
    {
      aom_api::EnvStartRequest env_req;

      env_req.set_session_id(session_id_string);
      env_req.mutable_user_request()->CopyFrom(*req);

      auto env_start_completion = make_rpc_completion<aom_api::EnvStartReply>(
          [this, self](auto rep, auto status) {
            env_start_reply = std::move(rep);
            env_start_status = status;

            recv();
          });

      auto rpc = owner_->env_stub_->AsyncStart(
          &env_start_completion->context, env_req, owner_->completion_queue_);
      env_start_completion->bind(*rpc);
      ++pending_subcalls;
      rpc.reset();
    }

    {
      aom_api::AgentStartRequest agent_req;

      agent_req.set_session_id(session_id_string);
      agent_req.mutable_user_request()->CopyFrom(*req);

      auto agent_start_completion =
          make_rpc_completion<aom_api::AgentStartReply>(
              [this, self](auto rep, auto status) {
                agent_start_reply = std::move(rep);
                agent_start_status = status;

                recv();
              });

      auto rpc =
          owner_->agent_stub_->AsyncStart(&agent_start_completion->context,
                                          agent_req, owner_->completion_queue_);
      agent_start_completion->bind(*rpc);
      ++pending_subcalls;
    }
  }

 private:
  Orchestrator* owner_;
  cb_t cb_;

  int pending_subcalls = 0;

  // Results of the environment start rpc
  ::aom_api::EnvStartReply env_start_reply;
  grpc::Status env_start_status;

  // Results of the agent start rpc
  ::aom_api::AgentStartReply agent_start_reply;
  grpc::Status agent_start_status;

  std::unique_ptr<Trial> new_trial_;
};

void Orchestrator::handle_Start(
    ::grpc::ServerContext* context, const ::aom_api::TrialStartRequest* request,
    std::function<void(grpc::Status, ::aom_api::TrialStartReply)> on_complete) {
  auto handler =
      std::make_shared<Trial_start_handler>(this, std::move(on_complete));
  handler->begin(request);
}

Orchestrator::Orchestrator(aom_api::Environment::StubInterface& env_stub,
                           aom_api::Agent::StubInterface& agent_stub,
                           grpc::CompletionQueue& cq)
    : env_stub_(&env_stub), agent_stub_(&agent_stub), completion_queue_(&cq) {}

struct Trial_start_listener {
  Trial_start_listener() : responder(&ctx) {}
  grpc::ServerContext ctx;
  aom_api::TrialStartRequest request;
  grpc::ServerAsyncResponseWriter<::aom_api::TrialStartReply> responder;
};

void Orchestrator::register_in_async_service(
    aom_api::Trial::AsyncService* service, grpc::ServerCompletionQueue* scq) {
  auto l = std::make_shared<Trial_start_listener>();

  service->RequestStart(
      &l->ctx, &l->request, &l->responder, completion_queue_, scq,
      make_completion([this, l, service, scq]() mutable {
        register_in_async_service(service, scq);

        this->handle_Start(&l->ctx, &l->request, [l](auto s, auto rep) {
          if (s.ok()) {
            l->responder.Finish(rep, s, make_completion([l]() {}));
          } else {
            l->responder.FinishWithError(s, make_completion([l]() {}));
          }
        });
      }));
}

}  // namespace aom
