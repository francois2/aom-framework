#ifndef AOM_ORCHESTRATOR_COMPLETION_H
#define AOM_ORCHESTRATOR_COMPLETION_H

#include <spdlog/spdlog.h>
#include <functional>

#include <grpcpp/grpcpp.h>
#include <grpcpp/support/async_unary_call.h>

namespace aom {

// This is meant for use with GRPC's Server Completion queues.
class Completion {
 public:
  virtual ~Completion() {}
  void exec() {
    proceed();
    delete this;
  }

 protected:
  virtual void proceed() = 0;
};

// A simple callback. Roughly analogous to a std::function<void()>
// make_completion should be used to create it.
template <typename CB_t>
class Cb_completion : public Completion {
 public:
  Cb_completion(CB_t cb) : cb_(std::move(cb)) {}

  void proceed() override { cb_(); }

 private:
  CB_t cb_;
};

template <typename T>
Completion* make_completion(T&& cb) {
  return new Cb_completion<T>(std::forward<T>(cb));
}

template <typename RepT, typename CbT>
class Rpc_completion : public Completion {
 public:
  Rpc_completion(CbT cb) : cb_(std::move(cb)) {}

  void bind(::grpc::ClientAsyncResponseReaderInterface<RepT>& rpc) {
    rpc.Finish(&reply, &status, this);
  }

  void proceed() override { cb_(std::move(reply), std::move(status)); }

  RepT reply;
  grpc::Status status;
  grpc::ClientContext context;

  CbT cb_;
};

template <typename RepT, typename CbT>
Rpc_completion<RepT, CbT>* make_rpc_completion(CbT&& cb) {
  return new Rpc_completion<RepT, CbT>(std::forward<CbT>(cb));
}

}  // namespace aom

#endif