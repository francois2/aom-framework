#ifndef AOM_ORCHESTRATOR_ORCHESTRATOR_H
#define AOM_ORCHESTRATOR_ORCHESTRATOR_H

#include <grpcpp/grpcpp.h>
#include "aom_framework/services.grpc.pb.h"

#include "aom/trial.h"

namespace aom {

struct Trial_start_handler;

class Orchestrator {
  aom_api::Environment::StubInterface* env_stub_ = nullptr;
  aom_api::Agent::StubInterface* agent_stub_ = nullptr;
  grpc::CompletionQueue* completion_queue_ = nullptr;

  std::unordered_map<uuids::uuid, std::unique_ptr<Trial>> trials_;

 public:
  Orchestrator(aom_api::Environment::StubInterface& env_stub,
               aom_api::Agent::StubInterface& agent_stub,
               grpc::CompletionQueue& cq);

  using Start_cb =
      std::function<void(grpc::Status, ::aom_api::TrialStartReply)>;

  void handle_Start(::grpc::ServerContext* context,
                    const ::aom_api::TrialStartRequest* request,
                    Start_cb on_complete);

  void register_in_async_service(aom_api::Trial::AsyncService* service,
                                 grpc::ServerCompletionQueue* scq);

 private:
  friend class Trial_start_handler;

  Orchestrator(const Orchestrator&) = delete;
  Orchestrator(Orchestrator&&) = delete;
  Orchestrator& operator=(const Orchestrator&) = delete;
  Orchestrator& operator=(Orchestrator&&) = delete;
};

}  // namespace aom

#endif