#ifndef AOM_ORCHESTRATOR_TRIAL_H
#define AOM_ORCHESTRATOR_TRIAL_H

#include "uuid.h"

namespace aom {
class Trial {
  uuids::uuid id_;
  static uuids::uuid_system_generator id_generator_;

 public:
  Trial();
  ~Trial();
  Trial(Trial&&) = default;
  Trial(const Trial&) = delete;
  Trial& operator=(Trial&&) = default;
  Trial& operator=(const Trial&) = delete;

  const uuids::uuid& id() { return id_; }
};
}  // namespace aom
#endif