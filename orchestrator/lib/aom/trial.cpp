#include "aom/trial.h"
#include "spdlog/spdlog.h"

namespace aom {
uuids::uuid_system_generator Trial::id_generator_;

Trial::Trial() : id_(id_generator_()) {
  spdlog::info("creating trial: {}", to_string(id_));
}

Trial::~Trial() { spdlog::info("tearing down trial: {}", to_string(id_)); }
}  // namespace aom