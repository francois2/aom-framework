FROM alpine:3.9

LABEL org.label-schema.name="aom-framework-builder" \
      org.label-schema.vendor="Age of Minds" \
      org.label-schema.url="https://registry.gitlab.com/age-of-minds/aom-framework:builder" \
      org.label-schema.vcs-url="https://gitlab.com/age-of-minds/aom-framework"

# version tags
ARG GRPC_RELEASE_TAG=v1.19.1
ARG GRPC_WEB_RELEASE=https://github.com/grpc/grpc-web/releases/download/1.0.4/protoc-gen-grpc-web-1.0.4-linux-x86_64

RUN apk add --no-cache \
  autoconf pkgconfig libtool automake g++ make git clang

RUN git clone -b ${GRPC_RELEASE_TAG} --single-branch https://github.com/grpc/grpc /var/local/src/grpc && \
    cd /var/local/src/grpc && \
    git submodule update --init --recursive && \
    echo "--- installing protobuf ---" && \
    cd /var/local/src/grpc/third_party/protobuf && \
    ./autogen.sh && ./configure --enable-shared && \
    make -j$(nproc) && make -j$(nproc) check && make install && make clean && ldconfig / && \
    echo "--- installing grpc ---" && \
    cd /var/local/src/grpc && \
    make CXXFLAGS="-Wno-error=class-memaccess -Wno-error=ignored-qualifiers -Wno-error=stringop-truncation" CFLAGS="-Wno-error=class-memaccess -Wno-error=ignored-qualifiers -Wno-error=stringop-truncation -Wno-error=sizeof-pointer-memaccess -Wno-error=stringop-overflow= -Wno-error=cast-function-type -Wno-error=implicit-fallthrough= -Wno-error=implicit-function-declaration" -j$(nproc) && \
    make install && make clean && ldconfig / && \
    rm -rf /var/local/src/grpc

RUN wget -O /usr/local/bin/protoc-gen-grpc_web ${GRPC_WEB_RELEASE} && \
    chmod +x /usr/local/bin/protoc-gen-grpc_web

RUN ln -s /usr/local/bin/grpc_cpp_plugin /usr/local/bin/protoc-gen-grpc_cpp && \
    ln -s /usr/local/bin/grpc_python_plugin /usr/local/bin/protoc-gen-grpc_python

WORKDIR /app