'use strict';

var protos = require('./aom_framework/protocols_pb.js');
var services = require('./aom_framework/services_grpc_web_pb.js');

module.exports.protos = protos;

module.exports.TrialClient = services.TrialClient;