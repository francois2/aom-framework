/**
 * @fileoverview gRPC-Web generated client stub for aom_api
 * @enhanceable
 * @public
 */

// GENERATED CODE -- DO NOT EDIT!



const grpc = {};
grpc.web = require('grpc-web');


var aom_framework_protocols_pb = require('../aom_framework/protocols_pb.js')
const proto = {};
proto.aom_api = require('./services_pb.js');

/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.aom_api.EnvironmentClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'binary';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

  /**
   * @private @const {?Object} The credentials to be used to connect
   *    to the server
   */
  this.credentials_ = credentials;

  /**
   * @private @const {?Object} Options for the client
   */
  this.options_ = options;
};


/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.aom_api.EnvironmentPromiseClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'binary';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

  /**
   * @private @const {?Object} The credentials to be used to connect
   *    to the server
   */
  this.credentials_ = credentials;

  /**
   * @private @const {?Object} Options for the client
   */
  this.options_ = options;
};


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.aom_api.EnvStartRequest,
 *   !proto.aom_api.EnvStartReply>}
 */
const methodInfo_Environment_Start = new grpc.web.AbstractClientBase.MethodInfo(
  aom_framework_protocols_pb.EnvStartReply,
  /** @param {!proto.aom_api.EnvStartRequest} request */
  function(request) {
    return request.serializeBinary();
  },
  aom_framework_protocols_pb.EnvStartReply.deserializeBinary
);


/**
 * @param {!proto.aom_api.EnvStartRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.aom_api.EnvStartReply)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.aom_api.EnvStartReply>|undefined}
 *     The XHR Node Readable Stream
 */
proto.aom_api.EnvironmentClient.prototype.start =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/aom_api.Environment/Start',
      request,
      metadata || {},
      methodInfo_Environment_Start,
      callback);
};


/**
 * @param {!proto.aom_api.EnvStartRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.aom_api.EnvStartReply>}
 *     A native promise that resolves to the response
 */
proto.aom_api.EnvironmentPromiseClient.prototype.start =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/aom_api.Environment/Start',
      request,
      metadata || {},
      methodInfo_Environment_Start);
};


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.aom_api.EnvUpdateRequest,
 *   !proto.aom_api.EnvUpdateReply>}
 */
const methodInfo_Environment_Update = new grpc.web.AbstractClientBase.MethodInfo(
  aom_framework_protocols_pb.EnvUpdateReply,
  /** @param {!proto.aom_api.EnvUpdateRequest} request */
  function(request) {
    return request.serializeBinary();
  },
  aom_framework_protocols_pb.EnvUpdateReply.deserializeBinary
);


/**
 * @param {!proto.aom_api.EnvUpdateRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.aom_api.EnvUpdateReply)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.aom_api.EnvUpdateReply>|undefined}
 *     The XHR Node Readable Stream
 */
proto.aom_api.EnvironmentClient.prototype.update =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/aom_api.Environment/Update',
      request,
      metadata || {},
      methodInfo_Environment_Update,
      callback);
};


/**
 * @param {!proto.aom_api.EnvUpdateRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.aom_api.EnvUpdateReply>}
 *     A native promise that resolves to the response
 */
proto.aom_api.EnvironmentPromiseClient.prototype.update =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/aom_api.Environment/Update',
      request,
      metadata || {},
      methodInfo_Environment_Update);
};


/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.aom_api.TrialClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'binary';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

  /**
   * @private @const {?Object} The credentials to be used to connect
   *    to the server
   */
  this.credentials_ = credentials;

  /**
   * @private @const {?Object} Options for the client
   */
  this.options_ = options;
};


/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.aom_api.TrialPromiseClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'binary';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

  /**
   * @private @const {?Object} The credentials to be used to connect
   *    to the server
   */
  this.credentials_ = credentials;

  /**
   * @private @const {?Object} Options for the client
   */
  this.options_ = options;
};


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.aom_api.TrialStartRequest,
 *   !proto.aom_api.TrialStartReply>}
 */
const methodInfo_Trial_Start = new grpc.web.AbstractClientBase.MethodInfo(
  aom_framework_protocols_pb.TrialStartReply,
  /** @param {!proto.aom_api.TrialStartRequest} request */
  function(request) {
    return request.serializeBinary();
  },
  aom_framework_protocols_pb.TrialStartReply.deserializeBinary
);


/**
 * @param {!proto.aom_api.TrialStartRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.aom_api.TrialStartReply)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.aom_api.TrialStartReply>|undefined}
 *     The XHR Node Readable Stream
 */
proto.aom_api.TrialClient.prototype.start =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/aom_api.Trial/Start',
      request,
      metadata || {},
      methodInfo_Trial_Start,
      callback);
};


/**
 * @param {!proto.aom_api.TrialStartRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.aom_api.TrialStartReply>}
 *     A native promise that resolves to the response
 */
proto.aom_api.TrialPromiseClient.prototype.start =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/aom_api.Trial/Start',
      request,
      metadata || {},
      methodInfo_Trial_Start);
};


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.aom_api.TrialActionRequest,
 *   !proto.aom_api.TrialActionReply>}
 */
const methodInfo_Trial_Action = new grpc.web.AbstractClientBase.MethodInfo(
  aom_framework_protocols_pb.TrialActionReply,
  /** @param {!proto.aom_api.TrialActionRequest} request */
  function(request) {
    return request.serializeBinary();
  },
  aom_framework_protocols_pb.TrialActionReply.deserializeBinary
);


/**
 * @param {!proto.aom_api.TrialActionRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.aom_api.TrialActionReply)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.aom_api.TrialActionReply>|undefined}
 *     The XHR Node Readable Stream
 */
proto.aom_api.TrialClient.prototype.action =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/aom_api.Trial/Action',
      request,
      metadata || {},
      methodInfo_Trial_Action,
      callback);
};


/**
 * @param {!proto.aom_api.TrialActionRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.aom_api.TrialActionReply>}
 *     A native promise that resolves to the response
 */
proto.aom_api.TrialPromiseClient.prototype.action =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/aom_api.Trial/Action',
      request,
      metadata || {},
      methodInfo_Trial_Action);
};


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.aom_api.TrialRewardRequest,
 *   !proto.aom_api.TrialRewardReply>}
 */
const methodInfo_Trial_Reward = new grpc.web.AbstractClientBase.MethodInfo(
  aom_framework_protocols_pb.TrialRewardReply,
  /** @param {!proto.aom_api.TrialRewardRequest} request */
  function(request) {
    return request.serializeBinary();
  },
  aom_framework_protocols_pb.TrialRewardReply.deserializeBinary
);


/**
 * @param {!proto.aom_api.TrialRewardRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.aom_api.TrialRewardReply)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.aom_api.TrialRewardReply>|undefined}
 *     The XHR Node Readable Stream
 */
proto.aom_api.TrialClient.prototype.reward =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/aom_api.Trial/Reward',
      request,
      metadata || {},
      methodInfo_Trial_Reward,
      callback);
};


/**
 * @param {!proto.aom_api.TrialRewardRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.aom_api.TrialRewardReply>}
 *     A native promise that resolves to the response
 */
proto.aom_api.TrialPromiseClient.prototype.reward =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/aom_api.Trial/Reward',
      request,
      metadata || {},
      methodInfo_Trial_Reward);
};


/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.aom_api.AgentClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'binary';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

  /**
   * @private @const {?Object} The credentials to be used to connect
   *    to the server
   */
  this.credentials_ = credentials;

  /**
   * @private @const {?Object} Options for the client
   */
  this.options_ = options;
};


/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.aom_api.AgentPromiseClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'binary';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

  /**
   * @private @const {?Object} The credentials to be used to connect
   *    to the server
   */
  this.credentials_ = credentials;

  /**
   * @private @const {?Object} Options for the client
   */
  this.options_ = options;
};


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.aom_api.AgentStartRequest,
 *   !proto.aom_api.AgentStartReply>}
 */
const methodInfo_Agent_Start = new grpc.web.AbstractClientBase.MethodInfo(
  aom_framework_protocols_pb.AgentStartReply,
  /** @param {!proto.aom_api.AgentStartRequest} request */
  function(request) {
    return request.serializeBinary();
  },
  aom_framework_protocols_pb.AgentStartReply.deserializeBinary
);


/**
 * @param {!proto.aom_api.AgentStartRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.aom_api.AgentStartReply)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.aom_api.AgentStartReply>|undefined}
 *     The XHR Node Readable Stream
 */
proto.aom_api.AgentClient.prototype.start =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/aom_api.Agent/Start',
      request,
      metadata || {},
      methodInfo_Agent_Start,
      callback);
};


/**
 * @param {!proto.aom_api.AgentStartRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.aom_api.AgentStartReply>}
 *     A native promise that resolves to the response
 */
proto.aom_api.AgentPromiseClient.prototype.start =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/aom_api.Agent/Start',
      request,
      metadata || {},
      methodInfo_Agent_Start);
};


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.aom_api.AgentDecideRequest,
 *   !proto.aom_api.AgentDecideReply>}
 */
const methodInfo_Agent_Decide = new grpc.web.AbstractClientBase.MethodInfo(
  aom_framework_protocols_pb.AgentDecideReply,
  /** @param {!proto.aom_api.AgentDecideRequest} request */
  function(request) {
    return request.serializeBinary();
  },
  aom_framework_protocols_pb.AgentDecideReply.deserializeBinary
);


/**
 * @param {!proto.aom_api.AgentDecideRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.aom_api.AgentDecideReply)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.aom_api.AgentDecideReply>|undefined}
 *     The XHR Node Readable Stream
 */
proto.aom_api.AgentClient.prototype.decide =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/aom_api.Agent/Decide',
      request,
      metadata || {},
      methodInfo_Agent_Decide,
      callback);
};


/**
 * @param {!proto.aom_api.AgentDecideRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.aom_api.AgentDecideReply>}
 *     A native promise that resolves to the response
 */
proto.aom_api.AgentPromiseClient.prototype.decide =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/aom_api.Agent/Decide',
      request,
      metadata || {},
      methodInfo_Agent_Decide);
};


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.aom_api.AgentRewardRequest,
 *   !proto.aom_api.AgentRewardReply>}
 */
const methodInfo_Agent_Reward = new grpc.web.AbstractClientBase.MethodInfo(
  aom_framework_protocols_pb.AgentRewardReply,
  /** @param {!proto.aom_api.AgentRewardRequest} request */
  function(request) {
    return request.serializeBinary();
  },
  aom_framework_protocols_pb.AgentRewardReply.deserializeBinary
);


/**
 * @param {!proto.aom_api.AgentRewardRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.aom_api.AgentRewardReply)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.aom_api.AgentRewardReply>|undefined}
 *     The XHR Node Readable Stream
 */
proto.aom_api.AgentClient.prototype.reward =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/aom_api.Agent/Reward',
      request,
      metadata || {},
      methodInfo_Agent_Reward,
      callback);
};


/**
 * @param {!proto.aom_api.AgentRewardRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.aom_api.AgentRewardReply>}
 *     A native promise that resolves to the response
 */
proto.aom_api.AgentPromiseClient.prototype.reward =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/aom_api.Agent/Reward',
      request,
      metadata || {},
      methodInfo_Agent_Reward);
};


module.exports = proto.aom_api;

