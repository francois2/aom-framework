SHELL:=/bin/sh
REGISTRY_IMAGE:=registry.gitlab.com/age-of-minds/aom-framework
all: protos

protos: messages services

messages: orchestrator/protos/aom_framework/protocols.proto aom-framework-builder
	docker run -it --rm -v $(shell pwd):/app ${REGISTRY_IMAGE}:builder protoc -I./orchestrator/protos \
	--python_out=./sdk_python \
	--js_out=import_style=commonjs,binary:./sdk_js \
	--cpp_out=./sdk_cpp \
	$<

services: orchestrator/protos/aom_framework/services.proto aom-framework-builder
	docker run -it --rm -v $(shell pwd):/app ${REGISTRY_IMAGE}:builder protoc -I./orchestrator/protos \
		--cpp_out=./sdk_cpp \
		--grpc_cpp_out=generate_mock_code=true:./sdk_cpp \
		--js_out=import_style=commonjs,binary:./sdk_js \
		--grpc_web_out=import_style=commonjs,mode=grpcweb:./sdk_js \
		--grpc_python_out=./sdk_python \
		--python_out=./sdk_python \
		$<

aom-framework-builder: Dockerfile.builder
	docker pull ${REGISTRY_IMAGE}:builder || true
	docker build -f $< --cache-from ${REGISTRY_IMAGE}:builder -t ${REGISTRY_IMAGE}:builder .

aom-framework-cpp-builder: Dockerfile.cpp_builder aom-framework-builder
	docker pull ${REGISTRY_IMAGE}:cpp_builder || true
	docker build -f $< --cache-from ${REGISTRY_IMAGE}:cpp_builder -t ${REGISTRY_IMAGE}:cpp_builder .

# This builds the production-ready docker image for the orchestrator
orchestrator: Dockerfile.orchestrator aom-framework-cpp-builder
	docker build -f $< -t ${REGISTRY_IMAGE}/aom-orchestrator .

cpp_files = orchestrator/main.cpp \
  orchestrator/lib/aom/completion.h \
  orchestrator/lib/aom/orchestrator.cpp \
  orchestrator/lib/aom/orchestrator.h \
  orchestrator/lib/aom/trial.h \
  orchestrator/lib/aom/trial.cpp \
  orchestrator/tests/grpc_mock_utils.h \
  orchestrator/tests/tests.cpp

cpp_format: $(cpp_files)
	docker run --rm --user $(shell id -u ${USER}):$(shell id -g ${USER}) -v $(shell pwd):/app ${REGISTRY_IMAGE}:builder clang-format -i -style=file $^